APP=tempgraph
VERSION=1.40

all:
	g++ -O3 -o $(APP) src/*.cpp -Isrc

debug:
	g++ -g -o $(APP) src/*.cpp -Isrc

source:
	tar -cvzf ../$(APP)_$(VERSION).orig.tar.gz ../$(APP)-$(VERSION) --exclude-vcs

install:
	install -m 755 $(APP) /usr/bin/$(APP)
	install -m 644 man/$(APP).1.gz /usr/share/man/man1/$(APP).1.gz

clean:
	rm -f $(APP)
