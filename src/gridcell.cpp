/*
  Grid cell representing an area of the globe
  Copyright (C) 2009-2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gridcell.h"

gridcell::gridcell() {
}

gridcell::~gridcell() {
}

float gridcell::get_average_temperature(int year)
{
    int hits=0;
    float average=0;

    for (int i = 0; i < (int)samples[year].size(); i++) {
        average += samples[year][i].temperature;
        hits++;
    }
    if (hits>0) {
        return average/hits;
    }
    return -9999.0f;
}

float gridcell::get_average_temperature(int year, int month)
{
    int hits=0;
    float average=0;

    for (int i = 0; i < (int)samples[year].size(); i++) {
        if (samples[year][i].month==month) {
            average += samples[year][i].temperature;
            hits++;
        }
    }
    if (hits>0) {
        return average/hits;
    }
    return -9999.0f;
}

float gridcell::get_average_temperature(int start_year, int end_year, int month)
{
    int hits=0;
    float average=0;

    for (int y = start_year; y <= end_year; y++) {
        for (int i = 0; i < (int)samples[y].size(); i++) {
            if (samples[y][i].month==month) {
                average += samples[y][i].temperature;
                hits++;
            }
        }
    }
    if (hits>0) {
        return average/hits;
    }
    return -9999.0f;
}

void gridcell::update(int reference_start_year, int reference_end_year)
{
    for (int m = 0; m < 12; m++) {
        reference_temperature[m] = get_average_temperature(reference_start_year, reference_end_year, m);
    }
}

bool gridcell::update_histogram(int year)
{
    for (int i = 0; i < 200; i++) histogram[i]=0;

    int tot=0;
    for (int s = 0; s < (int)samples[year].size(); s++) {
        int temp_index = (int)(samples[year][s].temperature+0.5f)+100;
        if ((temp_index>=0) &&
            (temp_index<200)) {
            histogram[temp_index]+=1.0f;
            tot++;
        }
    }

    // Normalise
    if (tot>0) {
        for (int i = 0; i < 200; i++) {
            histogram[i] = histogram[i]*100.0f/tot;
        }
        return true;
    }
    return false;
}

float gridcell::get_anomaly(int year, int month,
                            int reference_start_year, int reference_end_year)
{
    if (reference_temperature[month]>-9999.0f) {
        float temperature = get_average_temperature(year, month);
        if (temperature > -9999.0f) {
            return get_average_temperature(year, month) - reference_temperature[month];
        }
    }
    return -9999.0f;
}

float gridcell::get_anomaly(int year,
                            int reference_start_year, int reference_end_year)
{
    int hits=0;
    float average=0;

    for (int m = 0; m < 12; m++) {
        float anomaly = get_anomaly(year,m,reference_start_year,reference_end_year);
        if (anomaly>-9999) {
            average += anomaly;
            hits++;
        }
    }
    if (hits>0) {
        return average/hits;
    }
    return -9999.0f;
}

void gridcell::get_variance(int year, float &min, float &max,
                            int reference_start_year, int reference_end_year)
{
    for (int m = 0; m < 12; m++) {
        float anomaly = get_anomaly(year,m,reference_start_year,reference_end_year);
        if (anomaly > -9999.0f) {
            if (anomaly<min) min = anomaly;
            if (anomaly>max) max = anomaly;
        }
    }
}
