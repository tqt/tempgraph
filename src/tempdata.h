/*
  tempgraph
  A command line utility for plotting temperature data
  Copyright (C) 2009-2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TEMPDATA_H_
#define TEMPDATA_H_

class tempdata {
public:
    int country_code;
    long long int station;
    int modifier;
    int year;
    int month;
    float temperature;
    char flag_data_measurement;
    char flag_quality_control;
    char flag_data_source;

    tempdata();
    virtual ~tempdata();
};

#endif /* TEMPDATA_H_ */
