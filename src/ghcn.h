/*
    functions useful for parsing GHCN data
    Copyright (C) 2009-2018 Bob Mottram
    bob@freedombone.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GHCN_H_
#define GHCN_H_

#include <stdio.h>
#include <vector>
#include <string.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <math.h>
#include "tempdata.h"
#include "stationdata.h"

using namespace std;

enum {
    PROPERTY_POPULATION_CLASS = 1
};

class ghcn {
private:
    static void get_stations_from_data(
        vector<tempdata> &data,
        vector<long long int> &stations,
        float min_temp, float max_temp,
        int start_month, int end_month,
        vector<long long int> &country_codes);

    static bool station_exists(vector<stationdata> &stations,
                               long long int number);
    static bool station_number_in_data(
        long long int station,
        vector<long long int> &stations_list);

    static void load_v2(
        string filename,
        int start_year,
        int end_year,
        vector<long long int> &country_codes,
        vector<long long int> &station_numbers,
        vector<tempdata> &data,
        int entity_type);

    static void load_v3(
        string filename,
        int start_year,
        int end_year,
        vector<long long int> &country_codes,
        vector<long long int> &station_numbers,
        vector<tempdata> &data,
        int entity_type);

    static void load_stations_v2(
        string filename,
        vector<stationdata> &stations,
        vector<tempdata> &data,
        vector<long long int> &country_codes,
        float min_temp, float max_temp,
        int start_month, int end_month);

    static void load_stations_v3(
        string filename,
        vector<stationdata> &stations,
        vector<tempdata> &data,
        vector<long long int> &country_codes,
        float min_temp, float max_temp,
        int start_month, int end_month,
        string population_class);
public:
    static bool station_has_property(int property_type,string value,
                                     long long int station_number,
                                     vector<stationdata> &stations);
    static bool FileExists(string filename);
    // Returns the location for the given station number
    static bool get_station_location(
        long long int station,
        vector<stationdata> &stations,
        float &north,
        float &west,
        float &altitude);

    // Returns true if the given station number is within the given area
    static bool station_within_area(
        long long int station_number,
        vector<stationdata> &stations,
        float latitudeN1,
        float longitudeW1,
        float latitudeN2,
        float longitudeW2);

    static bool vector_contains(vector<int> &list, int num_to_find);
    static bool vector_contains(vector<long long int> &list, long long int num_to_find);
    static void get_country_codes(
        vector<string> &find_countries,
        vector<long long int> &code,
        vector<string> &country,
        vector<long long int> &result);

    static void get_stations_within_area(
        float latitudeN1,
        float longitudeW1,
        float latitudeN2,
        float longitudeW2,
        vector<stationdata> &stations,
        vector<stationdata> &returned_station_numbers);

    static void load_stations(
        string filename,
        float ghcn_version,
        vector<stationdata> &stations,
        vector<tempdata> &data,
        vector<long long int> &country_codes,
        float min_temp, float max_temp,
        int start_month, int end_month,
        string population_class);

    static void save_stations(
        string kml_filename,
        vector<stationdata> &stations);

    static void load_country_codes(
        string filename,
        vector<long long int> &code,
        vector<string> &country);

    static void load(
        string filename,
        float version,
        int start_year,
        int end_year,
        vector<long long int> &country_codes,
        vector<long long int> &station_numbers,
        vector<tempdata> &data,
        int entity_type);

    ghcn();
    virtual ~ghcn();
};

#endif /* GHCN_H_ */
