/*
  Weather station data structure
  Copyright (C) 2009-2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STATIONDATA_H_
#define STATIONDATA_H_

#include <string>

using namespace std;

class stationdata {
public:
    long long int number;
    string name;
    float latitude,longitude,altitude;
    string population_class;
    int population_size;
    string topography_class;
    string vegetation_class;
    string water_class;
    int water_distance_metres;
    string airport_class;
    int population_distance_metres;
    string gridded_vegetation_class;
    string satellite_population_class;

    stationdata();
    virtual ~stationdata();
};

#endif /* STATIONDATA_H_ */
