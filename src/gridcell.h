/*
  Grid cell representing an area of the globe
  Copyright (C) 2009-2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GRIDCELL_H_
#define GRIDCELL_H_

#include <vector>
#include <stdio.h>
#include "tempdata.h"

using namespace std;

class gridcell {
public:
    // samples for each month
    vector<tempdata> samples[3000];
    float histogram[200];

    // 3D coordinates of the cell centre on a unit sphere
    float x,y,z;

    // Latitude and longitude of centre point
    float latitude,longitude;

    float reference_temperature[12];

    void update(int reference_start_year, int reference_end_year);
    bool update_histogram(int year);
    float get_average_temperature(int year);
    float get_average_temperature(int year, int month);
    float get_average_temperature(int start_year, int end_year, int month);
    float get_anomaly(int year, int month,
                      int reference_start_year, int reference_end_year);
    float get_anomaly(int year,
                      int reference_start_year, int reference_end_year);
    void get_variance(int year, float &min, float &max, int reference_start_year, int reference_end_year);

    gridcell();
    virtual ~gridcell();
};

#endif /* GRIDCELL_H_ */
