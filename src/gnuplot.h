/*
    plot climatology data using gnuplot
    Copyright (C) 2009-2018 Bob Mottram
    bob@freedombone.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GNUPLOT_H_
#define GNUPLOT_H_

#define ENTITY_COUNTRY 0
#define ENTITY_STATION 1

#include <stdio.h>
#include <vector>
#include <string.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "tempdata.h"
#include "gridcell.h"
#include "globalgrid.h"
#include "ghcn.h"

using namespace std;

class gnuplot {
private:
    static void save_world_map(string filename);

public:
    static int plot_annual(
        string plot_script_filename,
        string plot_data_filename,
        string countries_image_filename,
        string averages_image_filename,
        string stations_filename,
        string title,
        string subtitle, float indent, float vpos,
        vector<tempdata> &data,
        int entity_type,
        vector<long long int> &entity_codes,
        vector<string> &entity_names,
        int start_year,
        int end_year,
        float min_temp,
        float max_temp,
        bool show_minmax,
        bool best_fit_line,
        int image_width,
        int image_height);

    static void plot_by_country_annual(
        string plot_script_filename,
        string plot_data_filename,
        string countries_image_filename,
        string averages_image_filename,
        string stations_filename,
        string title,
        string subtitle, float indent, float vpos,
        vector<tempdata> &data,
        vector<long long int> &country_codes,
        vector<string> &country_names,
        int start_year,
        int end_year,
        float min_temp,
        float max_temp,
        bool show_minmax,
        bool best_fit_line,
        int image_width,
        int image_height);

    static void plot_by_station_annual(
        string plot_script_filename,
        string plot_data_filename,
        string stations_image_filename,
        string averages_image_filename,
        string title,
        string subtitle, float indent, float vpos,
        vector<tempdata> &data,
        vector<long long int> &station_numbers,
        vector<string> &station_names,
        int start_year,
        int end_year,
        bool show_minmax,
        bool best_fit_line,
        int image_width,
        int image_height);

    static int plot_active_stations(
        string plot_script_filename,
        string plot_data_filename,
        string active_stations_image_filename,
        string equator_dist_image_filename,
        string title_active_stations,
        string title_equator_dist,
        string subtitle, float indent, float vpos,
        vector<tempdata> &data,
        vector<long long int> &country_codes,
        vector<stationdata> &stations,
        int start_year,
        int end_year,
        float min_temp, float max_temp,
        string population_class,
        vector<float> &area,
        int image_width,
        int image_height);

    static int plot_annual_only(
        string plot_script_filename,
        string plot_data_filename,
        string averages_image_filename,
        string title,
        string subtitle, float indent, float vpos,
        vector<tempdata> &data,
        vector<long long int> &entity_codes,
        vector<string> &entity_names,
        int start_year,
        int end_year,
        bool show_minmax,
        bool best_fit_line,
        int image_width,
        int image_height);

    static int plot_anomalies(
        string plot_script_filename,
        string plot_data_filename,
        string anomalies_image_filename,
        string title,
        string subtitle, float indent, float vpos,
        vector<gridcell> &grid,
        int start_year,
        int end_year,
        int reference_start_year,
        int reference_end_year,
        bool show_minmax,
        bool show_running_average,
        bool best_fit_line,
        int image_width,
        int image_height);

    static int plot_distribution(
        string plot_script_filename,
        string plot_data_filename,
        string anomalies_image_filename,
        string title,
        string subtitle, float indent, float vpos,
        vector<gridcell> &grid,
        int start_year,
        int end_year,
        int reference_start_year,
        int reference_end_year,
        int interval,
        int image_width,
        int image_height);

    static int plot_world_map(
        string plot_script_filename,
        string plot_data_filename,
        string world_data_filename,
        string world_map_image_filename,
        string title,
        string subtitle, float indent, float vpos,
        vector<gridcell> &grid,
        int year,
        int reference_start_year,
        int reference_end_year,
        bool threed,
        float view_longitude, float view_latitude,
        bool show_cell_centres,
        int image_width,
        int image_height);

    static int plot_seasonal_anomalies(
        string plot_script_filename,
        string plot_data_filename,
        string anomalies_image_filename,
        string title,
        string subtitle, float indent, float vpos,
        vector<gridcell> &grid,
        int start_year,
        int end_year,
        int reference_start_year,
        int reference_end_year,
        bool show_running_average,
        int image_width,
        int image_height);

    static int plot_latitude_temperature(
        string plot_script_filename,
        string plot_data_filename,
        string latitude_temperature_filename,
        string title,
        string subtitle, float indent, float vpos,
        vector<tempdata> &data,
        vector<stationdata> &stations,
        int start_year,
        int end_year,
        string population_class,
        bool show_minmax,
        int image_width,
        int image_height);

    static int plot_altitude_temperature(
        string plot_script_filename,
        string plot_data_filename,
        string altitude_temperature_filename,
        string title,
        string subtitle, float indent, float vpos,
        vector<tempdata> &data,
        vector<stationdata> &stations,
        int start_year,
        int end_year,
        string population_class,
        bool show_minmax,
        int image_width,
        int image_height);

    static int plot_station_altitudes(
        string plot_script_filename,
        string plot_data_filename,
        string station_altitudes_filename,
        string title,
        string subtitle, float indent, float vpos,
        vector<tempdata> &data,
        vector<long long int> &country_codes,
        vector<stationdata> &stations,
        int start_year,
        int end_year,
        float min_temp, float max_temp,
        string population_class,
        vector<float> &area,
        int image_width,
        int image_height);

    static int plot_average_altitudes(
        string plot_script_filename,
        string plot_data_filename,
        string altitudes_filename,
        string title,
        string subtitle, float indent, float vpos,
        vector<tempdata> &data,
        vector<long long int> &country_codes,
        vector<stationdata> &stations,
        int start_year,
        int end_year,
        float min_temp, float max_temp,
        string population_class,
        vector<float> &area,
        bool show_running_average,
        int image_width,
        int image_height);

    gnuplot();
    virtual ~gnuplot();
};

#endif /* GNUPLOT_H_ */
