<img src="https://code.freedombone.net/bashrc/tempgraph/raw/master/images/Weather_stations_eu.png?raw=true" width=500/>

This is a commandline tool for educational or research purposes which generates graphs showing changes in average temperatures over time, using data from the Global Historical Climatology Network.

Also for temperatures below sea level see https://code.freedombone.net/bashrc/argograph

Installation
============

To compile from source first install the prerequisites:

On a Debian based system:

``` bash
sudo apt-get install build-essential gnuplot wget marble
```

On Arch/Parabola:

``` bash
sudo pacman -S gnuplot wget marble
```

To compile the command line program:

```bash
make
sudo make install
```


Obtaining the data
==================

Before you begin you'll need to download the latest version of the GHCN version 3 data.  tempgraph needs three files: the country codes, the weather stations and the temperature data itself.  These files can be obtained here:

```bash
cd data
wget ftp://ftp.ncdc.noaa.gov/pub/data/ghcn/v3/ghcnm.tavg.latest.qca.tar.gz
wget ftp://ftp.ncdc.noaa.gov/pub/data/ghcn/v3/country-codes
tar -xzvf ghcnm.tavg.latest.qca.tar.gz
cd ..
```

The compressed archive contains two files, one which is the temperature data (.dat) and the other which contains details of the weather stations (.inv). I typically rename these to:

```bash
cp data/ghcnm.*/*.dat data/v3.mean
cp data/ghcnm.*/*.inv data/wmo.txt
cp data/country-codes data/v3.country.codes
```

Usage
=====

For how to use the command see the manpage.

``` bash
man tempgraph
```
